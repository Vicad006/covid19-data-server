package org.covid19databank.repository;

import org.covid19databank.solr.LiteratureDoc;
import org.springframework.data.solr.repository.SolrCrudRepository;

public interface LiteratureDocRepository extends SolrCrudRepository<LiteratureDoc, Integer> {
	LiteratureDoc findByName(String name);
}
