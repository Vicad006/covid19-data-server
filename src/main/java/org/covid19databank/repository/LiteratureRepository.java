package org.covid19databank.repository;

import org.covid19databank.model.Literature;
import org.covid19databank.projection.StatProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LiteratureRepository extends JpaRepository<Literature, Integer> {

    List<Literature> findAllByAuthorIgnoreCase(String name);

    @Query("SELECT ltype.id as id, count(lit) as frequency, ltype.name as label FROM Literature lit" +
            " JOIN lit.literatureType ltype" +
            " group by id, label order by label asc")
    List<StatProjection> findFrequencyByLiteratureType();

    @Query(value = "SELECT year(publication_date) as label, count(*) AS frequency FROM literature " +
            "GROUP BY label " +
            "ORDER BY frequency DESC", nativeQuery = true)
    List<StatProjection> findFrequencyByPublicationDate();

    @Query(value = "SELECT journal as label, count(*) AS frequency" +
            " FROM literature" +
            " GROUP BY label" +
            " ORDER BY frequency DESC limit 10", nativeQuery = true)
    List<StatProjection> findFrequencyByJournal();

    @Query("SELECT lit.source as label, count(lit) AS frequency" +
            " FROM Literature lit" +
            " GROUP BY label" +
            " ORDER BY frequency DESC")
    List<StatProjection> findFrequencyBySource();


}
