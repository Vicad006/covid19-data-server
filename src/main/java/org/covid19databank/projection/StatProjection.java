package org.covid19databank.projection;

public interface StatProjection {
    Integer getFrequency();

    String getLabel();
}


