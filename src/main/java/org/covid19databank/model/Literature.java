package org.covid19databank.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"europePmcId", "literature_type_id"})})
public class Literature {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String europePmcId;

    @Column(columnDefinition = "Text")
    private String name;

    @Column(columnDefinition = "Text")
    private String author;

    @Column(columnDefinition = "Text")
    private String displayAuthor;

    private String pmcid;
    private String publicationDate;
    private String publicationYear;
    private String journal;
    private String volume;
    private String issue;
    private String pagination;

    @Column(columnDefinition = "Text")
    private String oRCID;
    private String source;

    @ManyToOne
    @JoinColumn(name = "literature_type_id")
    private LiteratureType literatureType;


}