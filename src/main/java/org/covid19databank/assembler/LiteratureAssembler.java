package org.covid19databank.assembler;

import org.covid19databank.controller.LiteratureController;
import org.covid19databank.dto.LiteratureDto;
import org.covid19databank.model.Literature;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class LiteratureAssembler extends RepresentationModelAssemblerSupport<Literature, LiteratureDto> {

    public LiteratureAssembler() {
        super(LiteratureController.class, LiteratureDto.class);
    }


    @Override
    public LiteratureDto toModel(Literature literature) {
        return LiteratureDto.builder()
                .name(literature.getName())
                .author(literature.getAuthor())
                .displayAuthor(literature.getDisplayAuthor())
                .publicationDate(literature.getPublicationDate())
                .journal(literature.getJournal())
                .volume(literature.getVolume())
                .issue(literature.getIssue())
                .pagination(literature.getPagination())
                .pmcid(literature.getPmcid())
                .oRCID(literature.getORCID())
                .source(literature.getSource())
                .literatureType(literature.getLiteratureType().getName())
                .build()
                .add(linkTo(
                        methodOn(LiteratureController.class)
                                .getOneLiterature(literature.getId())).withSelfRel());
    }


}



