package org.covid19databank.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.covid19databank.model.LiteratureType;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode(callSuper = false)
@Relation(collectionRelation = "literatures", itemRelation = "literature")
public class LiteratureDto extends RepresentationModel<LiteratureDto> {

    private String europePmcId;
    private String name;
    private String author;
    private String displayAuthor;
    private String pmcid;
    private String publicationDate;
    private String publicationYear;
    private String journal;
    private String volume;
    private String issue;
    private String pagination;
    private String oRCID;
    private String source;
    private String literatureType;

}
