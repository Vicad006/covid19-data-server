package org.covid19databank.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.covid19databank.projection.StatProjection;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StatisticsDto {

    @JsonProperty("literature type")
    private List<StatProjection> literatureTypes;

    @JsonProperty("publication year")
    private List<StatProjection> publicationYear;

    @JsonProperty("journal")
    private List<StatProjection> journal;

    @JsonProperty("source")
    private List<StatProjection> source;
}
