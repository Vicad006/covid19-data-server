package org.covid19databank.controller;

import org.covid19databank.assembler.LiteratureAssembler;
import org.covid19databank.dto.LiteratureDto;
import org.covid19databank.dto.StatisticsDto;
import org.covid19databank.model.Literature;
import org.covid19databank.services.LiteratureService;
import org.covid19databank.services.SolrIndexer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.data.web.SortDefault;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class LiteratureController {
    private LiteratureService literatureService;
    private LiteratureAssembler literatureAssembler;
    private SolrIndexer solrIndexer;

    public LiteratureController(LiteratureService literatureService,
                                LiteratureAssembler literatureAssembler,
                                SolrIndexer solrIndexer) {
        this.literatureService = literatureService;
        this.literatureAssembler = literatureAssembler;
        this.solrIndexer = solrIndexer;
    }

    @GetMapping("/literatures/{id}")
    public ResponseEntity<Object> getOneLiterature(@PathVariable Integer id) {
        Optional<Literature> literature = literatureService.getOne(id);
        if (literature.isPresent()) {
            EntityModel<Literature> resource = EntityModel.of(literature.get());
            resource.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(LiteratureController.class)).withSelfRel());
            return new ResponseEntity<>(resource, HttpStatus.OK);
        }
        return new ResponseEntity<>("Report " + id + " not found", HttpStatus.NOT_FOUND);
    }

    @GetMapping("/literatures")
    public ResponseEntity<Object> getAllLiteratures(PagedResourcesAssembler<Literature> assembler,
                                                    @SortDefault(sort = "name", direction = Sort.Direction.ASC)
                                                    @PageableDefault(size = 10, page = 0) Pageable pageable) {
        Page<Literature> literaturePage = literatureService.getAll(pageable);
        PagedModel<LiteratureDto> pagedModel = assembler.toModel(literaturePage, literatureAssembler);
        return new ResponseEntity<>(pagedModel, HttpStatus.OK);
    }


    @GetMapping("/author/{name}")
    public ResponseEntity<Object> getAuthor(@PathVariable String name) {
        List<Literature> literaturePage = literatureService.getAuthorByName(name);
        return new ResponseEntity<>(literaturePage, HttpStatus.OK);
    }

    @GetMapping("/index-data")
    public void getSolr() {
        solrIndexer.literatureIndex();
    }

    @GetMapping("/stat-count")
    public ResponseEntity<Object> getLiteratureTypeId() {

        StatisticsDto statisticsDto = literatureService.literatureStatistics();
        return new ResponseEntity<>(statisticsDto, HttpStatus.OK);
    }

}
