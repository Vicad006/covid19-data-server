package org.covid19databank.loader;

import org.covid19databank.services.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


@Component
public class InitDB implements CommandLineRunner {

    private Logger log = LoggerFactory.getLogger(InitDB.class);

    private InitDataBaseService initDataBaseService;
    private LiteratureService literatureService;

    @Value("${init.database}")
    private boolean iniitializeDatabase;

    @Value("${load.data}")
    private boolean loadData;

    public InitDB(InitDataBaseService initDataBaseService,
                  LiteratureService literatureService) {
        this.initDataBaseService = initDataBaseService;
        this.literatureService = literatureService;
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("System successfuly initialized, Is Database initializing ? User set it as: " + iniitializeDatabase);
        if (iniitializeDatabase) {
            log.info("Database intialization commenced");
            initDataBaseService.loadLiteratureTypes();
            log.info("Database intialization success");
        }

        if (loadData) {
            literatureService.cleanDatabase();
            log.info("Data Loading starting now");
            literatureService.getLiteratureData();
            log.info("Data Loading successfuly done");
        }
    }
}
