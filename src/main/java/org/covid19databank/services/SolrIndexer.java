package org.covid19databank.services;

import org.covid19databank.model.Literature;
import org.covid19databank.repository.LiteratureDocRepository;
import org.covid19databank.repository.LiteratureRepository;
import org.covid19databank.solr.LiteratureDoc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SolrIndexer {

    private Logger log = LoggerFactory.getLogger(SolrIndexer.class);

    private final LiteratureRepository literatureRepository;
    private final LiteratureDocRepository literatureDocRepository;

    public SolrIndexer(LiteratureRepository literatureRepository,
                       LiteratureDocRepository literatureDocRepository) {
        this.literatureRepository = literatureRepository;
        this.literatureDocRepository = literatureDocRepository;
    }

    public void literatureIndex() {
        literatureDocRepository.deleteAll();
        List<Literature> literatures = literatureRepository.findAll();
        for (Literature literature : literatures) {
            LiteratureDoc literatureDoc = LiteratureDoc.builder()
                    .id(literature.getId())
                    .name(literature.getName())
                    .author(literature.getAuthor())
                    .displayAuthor(literature.getDisplayAuthor())
                    .publicationDate(literature.getPublicationDate())
                    .journal(literature.getJournal())
                    .volume(literature.getVolume())
                    .issue(literature.getIssue())
                    .pagination(literature.getPagination())
                    .pmcid(literature.getPmcid())
                    .oRCID(literature.getORCID())
                    .source(literature.getSource())
                    .literatureType(literature.getLiteratureType().getName())
                    .build();
            log.info(literatureDoc.toString());

            literatureDocRepository.save(literatureDoc);
        }

    }
}
