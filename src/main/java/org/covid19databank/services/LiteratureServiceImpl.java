package org.covid19databank.services;

import org.covid19databank.dto.StatisticsDto;
import org.covid19databank.model.Literature;
import org.covid19databank.model.LiteratureType;
import org.covid19databank.payload.europepmc.Entry;
import org.covid19databank.payload.europepmc.ResearchData;
import org.covid19databank.projection.StatProjection;
import org.covid19databank.repository.LiteratureRepository;
import org.covid19databank.repository.LiteratureTypeRepository;
import org.covid19databank.constant.LiteratureTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class LiteratureServiceImpl implements LiteratureService {

    private final Logger log = LoggerFactory.getLogger(LiteratureServiceImpl.class);
    private final RestTemplate restTemplate;
    private final LiteratureRepository literatureRepository;
    private final LiteratureTypeRepository literatureTypeRepository;

    public LiteratureServiceImpl(LiteratureRepository literatureRepository,
                                 RestTemplate restTemplate,
                                 LiteratureTypeRepository literatureTypeRepository) {
        this.literatureRepository = literatureRepository;
        this.restTemplate = restTemplate;
        this.literatureTypeRepository = literatureTypeRepository;
    }

    @Override
    public void getLiteratureData() {
        List<LiteratureTypeEnum> typeEnums = Arrays.asList(LiteratureTypeEnum.values());
        typeEnums.forEach(typeEnum -> {
            String url = typeEnum.getUrl();
            String literatureTypeName = typeEnum.getType();

            ResearchData data = restTemplate.getForObject(url, ResearchData.class);
            log.info(url);
            List<Entry> entries = data.getEntries();
            int dataCount = data.getHitCount();

            int start = 1000;
            while (start <= dataCount) {
                log.info("Retrieving {}th data sets out of {} ", start, dataCount);
                data = restTemplate.getForObject(url + "&start=" + start, ResearchData.class);
                entries.addAll(data.getEntries());
                start += 1000;
            }

            loadLiteratureData(entries, literatureTypeName);
        });
    }

    @Override
    public void loadLiteratureData(List<Entry> entries, String literatureTypeName) {
        LiteratureType literatureType = literatureTypeRepository.findByName(literatureTypeName);
        List<Literature> literatureList = new ArrayList<>();
        for (Entry entry : entries) {
            try {
                String europePmcId = entry.getId();
                String name = entry.getFields().getName().get(0);
                String author = join(entry.getFields().getAuthor());
                String displayAuthor = join(entry.getFields().getDisplayAuthor());
                String publication = entry.getFields().getPublicationDate().get(0);
                String journal = join(entry.getFields().getJournal());
                String volume = join(entry.getFields().getVolume());
                String issue = join(entry.getFields().getIssue());
                String pagination = join(entry.getFields().getPagination());
                String pmcid = join(entry.getFields().getPmcid());
                String orcid = join(entry.getFields().getoRCID());
                String source = join(entry.getFields().getSource());
                log.info(name);

                Literature literature = Literature.builder()
                        .europePmcId(europePmcId)
                        .name(name)
                        .author(author)
                        .displayAuthor(displayAuthor)
                        .publicationDate(publication)
                        .publicationYear(publication.substring(4))
                        .journal(journal)
                        .volume(volume)
                        .issue(issue)
                        .pagination(pagination)
                        .pmcid(pmcid)
                        .oRCID(orcid)
                        .source(source)
                        .build();

                literatureList.add(literature);
                log.info("Loaded Publication : {}", europePmcId);
            } catch (Exception e) {
                log.info("Something is wrong with data {}", entry.getId());
            }
        }
        literatureRepository.saveAll(literatureList);
    }

    @Override
    public void cleanDatabase() {
        literatureRepository.deleteAll();
    }

    @Override
    public String join(List<String> list) {
        return String.join(",", list);
    }

    @Override
    public Optional<Literature> getOne(Integer id) {
        return literatureRepository.findById(id);
    }

    @Override
    public Page<Literature> getAll(Pageable pageable) {
        return literatureRepository.findAll(pageable);
    }

    public List<Literature> getAuthorByName(String name) {
        return literatureRepository.findAllByAuthorIgnoreCase(name);
    }

    public StatisticsDto literatureStatistics() {

        List<StatProjection> literatureTypeStat = literatureRepository.findFrequencyByLiteratureType();
        List<StatProjection> publicationDateStat = literatureRepository.findFrequencyByPublicationDate();
        List<StatProjection> journalStat = literatureRepository.findFrequencyByJournal();
        List<StatProjection> sourceStat = literatureRepository.findFrequencyBySource();

        return StatisticsDto.builder()
                .literatureTypes(literatureTypeStat)
                .publicationYear(publicationDateStat)
                .journal(journalStat)
                .source(sourceStat)
                .build();
    }


}
