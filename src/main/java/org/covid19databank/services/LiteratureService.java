package org.covid19databank.services;


import org.covid19databank.dto.StatisticsDto;
import org.covid19databank.model.Literature;
import org.covid19databank.payload.europepmc.Entry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface LiteratureService {

    void getLiteratureData();

    void loadLiteratureData(List<Entry> entries, String literatureTypeName);

    void cleanDatabase();

    String join(List<String> list);

    Optional<Literature> getOne(Integer id);

    Page<Literature> getAll(Pageable pageable);

    List<Literature> getAuthorByName(String name);

    StatisticsDto literatureStatistics();

}
