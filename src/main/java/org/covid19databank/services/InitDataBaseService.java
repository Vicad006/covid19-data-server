package org.covid19databank.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.covid19databank.model.LiteratureType;
import org.covid19databank.repository.*;
import org.covid19databank.constant.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class InitDataBaseService {

    private ObjectMapper mapper = new ObjectMapper();
    private Logger log = LoggerFactory.getLogger(InitDataBaseService.class);

    private LiteratureTypeRepository literatureTypeRepository;

    public InitDataBaseService(LiteratureTypeRepository literatureTypeRepository) {
        this.literatureTypeRepository = literatureTypeRepository;
    }

    public void loadLiteratureTypes() {
        List<LiteratureType> literatureTypes = new ArrayList<>();
        List<LiteratureTypeEnum> types = Arrays.asList(LiteratureTypeEnum.values());
        for (LiteratureTypeEnum type : types) {
            LiteratureType literatureType = new LiteratureType(type.getType());
            literatureTypes.add(literatureType);
        }
        literatureTypeRepository.deleteAll();
        literatureTypeRepository.saveAll(literatureTypes);
    }
}

