package org.covid19databank.solr;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.solr.client.solrj.beans.Field;
import org.covid19databank.model.LiteratureType;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

import javax.persistence.*;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@SolrDocument(collection = "literature")
public class LiteratureDoc {

    @Id
    @Indexed
    private int id;
    @Indexed
    private String europePmcId;
    @Indexed
    private String name;
    @Indexed
    private String author;
    @Indexed(name = "displayAuthor", type = "string")
    private String displayAuthor;
    @Indexed(name = "pmcid", type = "string")
    private String pmcid;
    @Indexed
    private String publicationDate;
    @Indexed
    private String journal;
    @Indexed
    @Field
    private String volume;
    @Indexed
    private String issue;
    @Indexed
    private String pagination;
    @Indexed
    private String oRCID;
    @Indexed
    private String source;
    @Indexed
    private String literatureType;

}